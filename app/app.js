'use strict';

angular.module('epr', [
	'ngSanitize',
	'epr.routes',
	'ngMaterial'
])
.run([
	'$rootScope',
	'$http',
	'$location',
function ($scope, $http, $location) {
	$scope.admin = false;
  $scope.user = false;

	$scope.logout = function() {
		$scope.admin = false;
		$scope.user = false;
	}
	// Expose app version info
	$http.get('version.json').success(function (v) {
		$scope.version = v.version;
		$scope.appName = v.name;
	});

}])
.config(function($mdThemingProvider,$mdIconProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('orange')
    .accentPalette('blue');
});

angular.module('epr.templates', []);
