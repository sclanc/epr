var app  = angular.module('epr')
app.controller('wardInCtr',['$scope','$mdDialog', function LoginCtr($scope,$mdDialog){

  $scope.eNames = {thing:'Kevin Johnson, David Whitmer'};
   $scope.cprNames = {thing:'William Law, Tommy Monson'};
   $scope.arNames = {thing:'Spencer Woodford,John Holland, Rodger Smallwood'};
   $scope.cNames = {thing:'Jones, Smith'};
   $scope.gNames = {thing:'Andersen, Johnsen, Monson'};
   $scope.sNames = {thing:'Hales, Holland, Smallwood'};
   $scope.q1 ={thing:'2'};
   $scope.q2 ={thing:'4'};
   $scope.q3 ={thing:'3'};
   $scope.edit2 =false;
   $scope.edit = false;

   $scope.toggleEdit2 = function(){
     if($scope.edit2 ===false){
       $scope.edit2 = true;
       return;
     }
     if($scope.edit2 ===true){
       $scope.edit2 = false;
       return;
     }
   }

  $scope.toggleEdit = function(){
    if($scope.edit ===false){
      $scope.edit = true;
      return;
    }
    if($scope.edit ===true){
      $scope.edit = false;
      return;
    }
  }
  $scope.showDialog =function() {
     var parentEl = angular.element(document.body);
     $mdDialog.show({
       parent: parentEl,
       template:
         '<md-dialog aria-label="Send Email">' +
         '  <md-dialog-content>'+
         '<h2>Send an Email to Ward Members<small><br>The email will send them a URL that will' +
         ' take them </br> to where they can update their personal profiles.</small></h2>'+
         '    <md-input-container>'+
         '    <label>Add a personal message:</label>'+
         '<textarea class="form-control input-lg" style="width: 500px; height:100px;"></textarea>'+
         '    </md-input-container>'+
         '  </md-dialog-content>' +
         '  <md-dialog-actions>' +
         '    <md-button ng-click="closeDialog()" class="md-primary">' +
         '      Send Email' +
         '    </md-button>' +
         '  </md-dialog-actions>' +
         '</md-dialog>',
       locals: {
         items: $scope.items
       },
       controller: DialogController
     });
      function DialogController($scope, $mdDialog) {
        $scope.closeDialog = function() {
          $mdDialog.hide();
        }
      }
    }

}]);
