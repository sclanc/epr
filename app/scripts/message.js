var app  = angular.module('epr')
app.controller('messageCtr',['$scope','$mdDialog', function messageCtr($scope, $mdDialog){
$scope.archives=[{subject:"Peterson's Car Crash",
message:"The Peterson family was in a car crash yesterday, Most of them are Okay, but we would like to send them some meals",
allMembers:"",
Bishopbric:"",
WardC:"",
rS:"Relief Society",
eQ:"",
hPG:"",
email:"email",
sms:"sms",
blizzard:"",
Tornado:"",
peterson:"Peterson Family Car Crash",
flood:"",
ss:""},{subject:"Blizzard",
message:"Everyone, this is the Bishopbric, please respond to let us know what your needs are with regard to the snow storm",
allMembers:"All Members",
Bishopbric:"",
WardC:"",
rS:"",
eQ:"",
hPG:"",
email:"",
sms:"",
blizzard:"Blizzard",
Tornado:"",
peterson:"",
flood:"",
ss:""}];


$scope.subject;
$scope.message;
$scope.allMembers;
$scope.Bishopbric;
$scope.WardC;
$scope.rS;
$scope.eQ;
$scope.hPG;
$scope.email;
$scope.sms;
$scope.blizzard;
$scope.Tornado;
$scope.peterson;
$scope.flood;
$scope.ss;

$scope.onSub = function(){
   var obj={subject:$scope.subject,
   message:$scope.message,
   allMembers:$scope.allMembers,
   Bishopbric:$scope.Bishopbric,
   WardC:$scope.WardC,
   rS:$scope.rS,
   eQ:$scope.eQ,
   hPG:$scope.hPG,
   email:$scope.email,
   sms:$scope.sms,
   blizzard:$scope.blizzard,
   Tornado:$scope.Tornado,
   peterson:$scope.peterson,
   flood:$scope.flood,
   ss:$scope.ss};
   $scope.archives.push(obj);
   console.log(obj);
}

$scope.showDetails = function(issue, event) {
   var parentEl = angular.element(document.body);
  $mdDialog.show({
    //parent: parentEl,
    templateUrl:'views/message.html',
    locals: {
      issue: issue
    },
    controller: DialogController
  });
   function DialogController($scope, $mdDialog) {
     $scope.message = issue.message;
     $scope.updates = false;
     $scope.selectedTime ="";
     $scope.times =[5,10,15,30,45,60,75];
     $scope.closeDialog = function() {
       $mdDialog.hide();
     }
   } //end of modal controller
 }//end of go to function

}]);
