var app  = angular.module('epr')
app.controller('pProfileCtr',['$scope', '$window','$mdDialog', function LoginCtr($scope, $window, $mdDialog){
$scope.item = {item:"", quant:"",remove:false};
$scope.origItem = {item:"", quant:"",remove:false};
$scope.items =[{item:'Chainsaw',quant:1,remove:false}, {item:'Shovels',quant:3,remove:false}, {item:'Quad',quant:1,remove:false},
{item:'Snow Blower',quant:1,remove:false}, {item:'Generator',quant:1,remove:false}, {item:'AA Batteries x16 pack',quant:2,remove:false}];
$scope.name= {thing:'Billy Thorton'};
$scope.num= {thing:'(800) 555-5555'};
$scope.add= {thing:'125 Some Cool Place'};
$scope.email= {thing:'bthort@gmail.com'};
$scope.edit = false;
$scope.editname = false;
$scope.editnum = false;
$scope.editadd = false;
$scope.editemail = false;
$scope.additem = false;

$scope.skill = {skill:"",details:"",cert:"",exp:""};
$scope.origSkill = {skill:"",details:"",cert:"",exp:""};
$scope.skills = [{skill:"CPR",details:"NA",cert:"American Red Cross",exp:"2/4/20",remove:false},
{skill:"CERT",details:"training in community emergency response",cert:"FEMA",exp:"6/13/16",remove:false},
{skill:"Fire starting",details:"Learned in Boy Scouts",cert:"NA",exp:"NA",remove:false},
{skill:"Winter weather training",details:"This is a in-app certification",cert:"EPR",exp:"4/2/20",remove:false}];
$scope.addSkill = false;
$scope.editSkill = false;

$scope.removeSkills = function() {
  $scope.skills = $scope.skills.filter(function(obj) {
     return (obj.remove !== true);
   });
}

$scope.addNewSkill = function() {
  $scope.skills.push(angular.copy($scope.skill));
  $scope.skill = angular.copy($scope.origItem);
  $scope.addSkill = false;
}


$scope.editSkills = function() {
  if($scope.editSkill ===true){
    $scope.editSkill = false;
    return;
  }
  if($scope.editSkill ===false){
    $scope.editSkill = true;
    return;
  }
}

$scope.checkListBool = false;
$scope.listTitle = "";
$scope.lists = [];
$scope.listItem = "";
$scope.oglistItem = "";
$scope.tempNewList = {
  items: []
};
$scope.ogtempNewList = {
  items: []
};

$scope.toggleCheckListBool = function() {
  if($scope.checkListBool ===true){
    $scope.checkListBool = false;
    return;
  }
  if($scope.checkListBool ===false){
    $scope.checkListBool = true;
    return;
  }
}

$scope.addNewItemToTempList = function(){
  $scope.tempNewList.items.push($scope.listItem)
  $scope.listItem = angular.copy($scope.oglistItem);
};

$scope.addNewlist = function() {
  $scope.tempNewList.title = $scope.listTitle;
  $scope.lists.push($scope.tempNewList);
  $scope.tempNewList = {};
    $scope.listTitle = angular.copy($scope.oglistItem);
      $scope.tempNewList = angular.copy($scope.ogtempNewList);
  $scope.checkListBool = false;
}

$scope.chooseLists = function() {
  alert = $mdDialog.alert({
       title: 'Choose from other Lists',
       textContent: 'This is where there would be other lists for people to choose from',
       ok: 'Close'
     });
     $mdDialog
       .show( alert )
       .finally(function() {
         alert = undefined;
       });
}


$scope.showCertInfo = function() {
  alert = $mdDialog.alert({
       title: 'Supplies and Tools',
       textContent: 'Examples of skills and certifications might include life guarding, cpr, carpentry, Ham Radio, and much more.'+
       ' You can gain certain skills and certifications by going to the Training page on this website. Checking the' +
       '"Make information available" checkbox will allow your Bishopbric to see the skills and certifications you have in the event of an emergency'+
       'and should only be checked if you feel confident in, and are willing to use those skills and certifications in an emergency.' +
       ' A skill will automatically be removed from the Bishopbric\'s view if it has expired.',
       ok: 'Close'
     });
     $mdDialog
       .show( alert )
       .finally(function() {
         alert = undefined;
       });
}

$scope.showInfo = function() {
  alert = $mdDialog.alert({
       title: 'Supplies and Tools',
       textContent: 'It can be helpful to keep a log of the items you have in the event of an emergency. Checking the' +
       '"Make information available" checkbox will allow your Bishopbric to see the items you have in the event of an emergency'+
       'and should only be checked if you are willing to make your supplies and tools available in an emergency.',
       ok: 'Close'
     });
     $mdDialog
       .show( alert )
       .finally(function() {
         alert = undefined;
       });
}

$scope.goToLdsTools  = function(){
           $window.open('https://ldsaccount.lds.org/signIn', '_blank');
       };

       $scope.removeItems = function() {
         $scope.items = $scope.items.filter(function(obj) {
            return (obj.remove !== true);
          });
       }

       $scope.addNew = function() {
         $scope.items.push(angular.copy($scope.item));
         $scope.item = angular.copy($scope.origItem);
         $scope.additem = false;
       }

$scope.toggleName = function(){
  if($scope.editname ===true){
    $scope.editname = false;
    return;
  }
  if($scope.editname ===false){
    $scope.editname = true;
    return;
  }
}

$scope.toggleNum = function(){
  if($scope.editnum ===true){
    $scope.editnum = false;
    return;
  }
  if($scope.editnum ===false){
    $scope.editnum =true;
    return;
  }
}

$scope.toggleAdd = function(){
  if($scope.editadd ===true){
    $scope.editadd = false;
    return;
  }
  if($scope.editadd ===false){
    $scope.editadd =true;
    return;
  }
}

$scope.toggleEmail = function(){

  if($scope.editemail ===true){
    $scope.editemail = false;
    return;
  }
  if($scope.editemail ===false){
    $scope.editemail =true;
    return;
  }
}

$scope.editItems = function() {
  console.log("edit!");
  if($scope.edit ===true){
    $scope.edit = false;
    return;
  }
  if($scope.edit ===false){
    $scope.edit = true;
    return;
  }
}
}]);
