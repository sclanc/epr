var app  = angular.module('epr')
app.controller('adminMainCtr',[ '$scope','$mdDialog',function($scope, $mdDialog) {
 $scope.addingIssue = false;
 $scope.issue = {name:"",subtasks:[]};
  $scope.orig= angular.copy($scope.issue);
  $scope.issues = [
    { name: 'Blizzard', img: 'img/100-0.jpeg', done: false, index:0, subtasks:[{content:"Shovel Sister Pensioner's Driveway "},
    {content:"Clear downed trees at the Bush's home "}]},
    { name: 'Tornado', img: 'img/100-1.jpeg', done: false, index:1, subtasks:[{content:""}] },
    { name: 'Peterson Family Car Crash', img: 'img/100-2.jpeg', done: false, index:2, subtasks:[{content:"Bring Dinners to their home"},{content:"Give the kids a ride to School"},
  {content:"Go to the hospital and give blessing"}] },
    { name: 'Flood', img: 'img/100-2.jpeg', done: false, index:3, subtasks:[{content:""}] },
    { name: 'School Shooting', img: 'img/100-2.jpeg', done: false, index:4, subtasks:[{content:""}] }
  ];
  $scope.goToIssue = function(issue, event) {
     var parentEl = angular.element(document.body);
    $mdDialog.show({
      //parent: parentEl,
      templateUrl:'views/issue.html',
      locals: {
        items: $scope.items,
        issue: issue
      },
      controller: DialogController
    });
     function DialogController($scope, $mdDialog) {
       $scope.subtasks = issue.subtasks;
       $scope.addingTask = false;
       $scope.task={content:""};
       $scope.orig= angular.copy($scope.task);
       $scope.closeDialog = function() {
         console.log($scope.addingTask);
         $mdDialog.hide();
       }
       $scope.removeSubTask = function(subtask,index){
            $scope.subtasks.splice(index,1);
          }
       $scope.addSubTask = function() {
          $scope.subtasks.push(angular.copy($scope.task));
          reset();
          $scope.addingTask = false;
       }
       $scope.addingSt = function() {
         if($scope.addingTask === false) {
           $scope.addingTask = true;
           return;
         }
         if($scope.addingTask === true) {
           $scope.addingTask = false;
           return;
         }
       }
       function reset(){
         $scope.task = angular.copy($scope.orig);
       }
     } //end of modal controller
   }//end of go to function

     $scope.remove = function(index) {
       $scope.issues = $scope.issues.filter(function(obj) {
          return (obj.done !== true);
        });
     }
     $scope.add = function() {
       $scope.issues.push(angular.copy($scope.issue));
       reset();
       $scope.addingIssue = false;
     }
     $scope.toggleIssue = function() {
       if($scope.addingIssue === false) {
         $scope.addingIssue = true;
         return;
       }
       if($scope.addingIssue === true) {
         $scope.addingIssue = false;
         return;
       }
     }
     function reset(){
       $scope.issue = angular.copy($scope.orig);
     }


}]);
