angular.module('epr.routes')

.config([
	'$routeProvider',
function ($routeProvider) {
	'use strict';
	$routeProvider
	.when('/login',{templateUrl: 'views/login.html', controller: 'LoginCtr'})
	.when('/sendMessage',{templateUrl: 'views/sendMessage.html', controller: 'messageCtr'})
	.when('/wardPlan',{templateUrl: 'views/wardPlan.html'})        
	.when('/pProfile',{templateUrl: 'views/pProfile.html',controller: 'pProfileCtr'})
	.when('/userMain',{templateUrl: 'views/userMain.html'})
	.when('/wardInventory',{templateUrl: 'views/wardInventory.html', controller: 'wardInCtr'})
	.when('/adminMain',{templateUrl: 'views/adminMain.html', controller: 'adminMainCtr'})
	.when('/training',{templateUrl: 'views/training.html',controller: 'trainCtr'})
	.when('/',{redirectTo:'/login'})
  .otherwise({redirectTo: '/#/'});
}]);
