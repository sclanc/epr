(function (angular, undefined) {
angular.module('epr.routes', [
	'epr.templates',
	'ngRoute'
]);
})(angular);
